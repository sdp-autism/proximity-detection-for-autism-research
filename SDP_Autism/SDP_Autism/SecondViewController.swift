//
//  SecondViewController.swift
//  SDP_Autism
//
//  Created by Anthony Foley on 2/13/16.
//  Copyright © 2016 BAM. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var WebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = NSURL(string: "http://chespin.baldwinc.com:1080/")
        let request = NSURLRequest(URL: url!)
        
        WebView.loadRequest(request)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

