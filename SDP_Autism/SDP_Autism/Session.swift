//
//  Session.swift
//  Proximity
//
//  Created by Baldwin Chang on 2/19/16.
//  Copyright © 2016 BAM. All rights reserved.
//

import Foundation

class Session {
    
    /*
        This class's main purpose is to
        build a framework for a class named
        Session
    */
    
    var state: String = "SDP_INIT"
    var ws: WebSocket!
    var view: SessionViewController!
    
    var host: String!
    var port: String!
    var password: String!
    var user: String!
    
    var session_id: Int!
    
    var connected: Bool!
    
    var last_update: Int! = 0
    
    var update_interval: Int = 5
    
    init(view: SessionViewController) {
        self.debug_message("Loaded session object")
        self.view = view
        
        self.connected = false
        
        let defaults = `NSUserDefaults`.standardUserDefaults()
        self.host = defaults.objectForKey("hostIPText") as? String
        self.port = defaults.objectForKey("portNumberText") as? String
        self.password = defaults.objectForKey("passwordText") as? String
        self.user = defaults.objectForKey("userIDText") as? String
        
        self.view.update_context_text("Connecting to \(self.host):\(self.port)...")
        
    }

    func start_connection(){
        
        self.ws = WebSocket("ws://\(self.host):\(self.port)")
        self.ws.event.open = {
            self.state = "SDP_CONNECTED"
            self.connected = true
            
            self.view.update_context_text("Authenticating...")
            self.sdp_send_password()
        }
        self.ws.event.close = { code, reason, clean in
            self.connected = false
            
            if self.state == "SDP_INIT" {
                self.view.alert_to_home("Connection Error", message: "Could not connect to the server using the connection details provided.\n\nCheck your details and try again.", confirmationText: "Dismiss")
            }
        }
        self.ws.event.error = { error in
            print("error \(error)")
        }
        self.ws.event.message = { message in
            if let resp = message as? String {
                self.fsm_response(resp)
            }
        }
    }
    
    func fsm_response(response: String) {
        if self.state == "SDP_CONNECTED" {
            self.sdp_connected(response)
        }
        else if self.state == "SDP_AWAITING_ID" {
            self.sdp_awaiting_id(response)
        }
        else if self.state == "SDP_AWAITING_SESSION" {
            self.sdp_awaiting_session(response)
        }
        else if self.state == "SDP_IN_SESSION" {
            self.sdp_in_session(response)
        }
    }
    
    // FSM Calls

    func sdp_send_password() {
        self.ws.send(self.password)
    }
    
    func sdp_connected(text: String) {
        if text == "IDENTIFY" {
            self.state = "SDP_AWAITING_ID"
            self.view.update_context_text("Authenticated! Now identifying as \(self.user)...")
            
            self.ws.send(self.user)
            
        }else{
            self.view.alert_to_home("Error", message: "Invalid server password! Please try again.", confirmationText: "Dismiss")
        }
    }
    
    func sdp_awaiting_id(text: String) {
        if text == "ID TAKEN" {
            self.view.alert_to_home("Error", message: "Unique identifier already taken. Please choose another name.", confirmationText: "Dismiss")
        }
        else if text.hasPrefix("IDENTIFIED.") {
            if text.hasSuffix("AWAITING SESSION.") {
                self.state = "SDP_AWAITING_SESSION"
                self.view.update_context_text("Awaiting session...")
            }else{
                self.view.hide_spinner()
                self.state = "SDP_IN_SESSION"
                let arr = text.componentsSeparatedByString("#")
                self.session_id = Int(arr[1])
                self.view.update_context_text("In Session #" + String(self.session_id))
            }
        }
    }
    
    func sdp_awaiting_session(text: String) {
        if text.hasPrefix("SESSION BEGIN:") {
            self.view.hide_spinner()
            self.state = "SDP_IN_SESSION"
            let arr = text.componentsSeparatedByString(": ")
            self.session_id = Int(arr[1])
            print(arr)
            self.view.update_context_text("In Session #" + String(self.session_id))
        }
    }
    
    func sdp_in_session(text: String) {
        if text == "SESSION END" {
            self.close_gracefully()
            self.view.update_context_text("Session has been ended.")
            self.view.update_button_text("Go back.")
            self.view.create_alert("Session Ended", message: "The session master has ended the session.", confirmationText: "Dismiss")
        }
    }
    
    func update(x: Double, y: Double, alpha: Double) {
        if self.state == "SDP_IN_SESSION" {
            let now: Int = Int(NSDate().timeIntervalSince1970)
            if (now - self.last_update) > self.update_interval {
                self.last_update = now
                let send: String = String(format: "REPORT %.2f %.2f %.1f", x, y, alpha)
                print(send)
                self.ws.send(send)
                self.view.update_label_text(String(format: "Last update: (%.2f, %.2f, %.1f)", x, y, alpha))
            }
        }
    }
    
    func close_gracefully() {
        if self.connected == true {
            self.ws.close()
        }
        // To be called when the application can decide to close the connection.
    }
    
    func debug_message(message: String) {
        print(String(format: "[SESSION] %@", message))
    }
    
}