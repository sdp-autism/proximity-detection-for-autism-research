//
//  SessionViewController.swift
//  SDP_Autism
//
//  Created by Baldwin Chang on 2/17/16.
//  
//  Product (of this file) Owner: Baldwin Chang
//

import UIKit

class SessionViewController: UIViewController, EILIndoorLocationManagerDelegate, UIAlertViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var EndSessionButton: UIButton!
    @IBOutlet weak var SessionHeader: UILabel!
    
    @IBOutlet weak var ContextStackView: UIStackView!
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ContextField: UILabel!
    @IBOutlet weak var LocationView: EILIndoorLocationView!
    @IBOutlet weak var UpdateLabel: UILabel!
    
    var state = "SDP_START"
    var ws : WebSocket!
    var session : Session!
    
    let locationManager = EILIndoorLocationManager()
    
    var location: EILLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.session = Session(view: self)
        
        self.session.start_connection()
        
        // Location
        // ODO: put your App ID and App Token here
        // You can get them by adding your app on https://cloud.estimote.com/#/apps
        ESTConfig.setupAppID("sdp-autism-kmf", andAppToken: "ff5a59aae947b5ea49625c76fe839a21")
        
        self.locationManager.delegate = self
        
        // ODO: replace with an identifier of your own location
        // You will find the identifier on https://cloud.estimote.com/#/locations
        let fetchLocationRequest = EILRequestFetchLocation(locationIdentifier: "mstb-124")
        fetchLocationRequest.sendRequestWithCompletion { (location, error) in
            if let location = location {
                self.location = location
                
                // You can configure the location view to your liking:
                self.LocationView.showTrace = true
                self.LocationView.rotateOnPositionUpdate = false
                // Consult the full list of properties on:
                // http://estimote.github.io/iOS-Indoor-SDK/Classes/EILIndoorLocationView.html
                
                self.LocationView.drawLocation(location)
                self.locationManager.startPositionUpdatesForLocation(self.location)
            } else {
                print("can't fetch location: \(error)")
            }
        }

        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func create_alert(title: String, message: String, confirmationText: String) {
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: confirmationText, style: UIAlertActionStyle.Default, handler: nil)
        
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func alert_to_home(title: String, message: String, confirmationText: String) {
        // will end any connection if there is one
        self.session.close_gracefully()
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let okAction = UIAlertAction(title: confirmationText, style: UIAlertActionStyle.Default, handler: { _ in
            self.performSegueWithIdentifier("ShowMainMenu", sender: nil)
        })
        
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func update_header_text(title: String) {
        self.SessionHeader.text = title
    }
    
    func update_button_text(text: String) {
        self.EndSessionButton.setTitle(text, forState: .Normal)
    }
    
    func update_context_text(text: String) {
        self.ContextField.text = text
    }
    
    func update_label_text(text: String) {
        self.UpdateLabel.text = text
    }
    
    func hide_spinner() {
        ActivityIndicator.alpha = 0.0
    }
    
    func indoorLocationManager(manager: EILIndoorLocationManager!, didUpdatePosition position: EILOrientedPoint!, withAccuracy positionAccuracy: EILPositionAccuracy, inLocation location: EILLocation!) {
        

        self.session.update(Double(position.x), y: Double(position.y), alpha: Double(position.orientation))
        //print(String(format: "x: %5.2f, y: %5.2f, orientation: %3.0f, accuracy: %@", position.x, position.y, position.orientation, accuracy))
        
        self.LocationView.updatePosition(position)
    }
    
    @IBAction func EndSessionButton(sender: UIButton, forEvent event: UIEvent) {
        self.session.close_gracefully()
    }

    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
