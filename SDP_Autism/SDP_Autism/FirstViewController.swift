//
//  FirstViewController.swift
//  SDP_Autism
//
//  Created by Anthony Foley on 2/13/16.
//  Copyright © 2016 BAM. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UIAlertViewDelegate, UITextFieldDelegate {


    
    // MARK: Properties
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var hostIPText: UITextField!
    @IBOutlet weak var userIDText: UITextField!
    @IBOutlet weak var portNumberText: UITextField!
    @IBOutlet weak var userIDLabel: UILabel!
    @IBOutlet weak var hostIPLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var portNumberLaberl: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Handle the user input through delegate callbacks.
        passwordText.delegate = self
        hostIPText.delegate = self
        userIDText.delegate = self
        portNumberText.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: Actions
    @IBAction func saveButtonClick(sender: AnyObject) {
        let defaults = `NSUserDefaults`.standardUserDefaults()
        
        defaults.setObject(userIDText.text, forKey: "userIDText")
        defaults.setObject(hostIPText.text, forKey: "hostIPText")
        defaults.setObject(passwordText.text, forKey: "passwordText")
        defaults.setObject(portNumberText.text, forKey: "portNumberText")
        defaults.synchronize()
        
        print("User=\(userIDText.text), Host=\(hostIPText.text), Password=\(passwordText.text)")
    }
    
    @IBAction func clearButtonClick(sender: AnyObject) {
        if(userIDText.text == ""){
            loadDefaults()
            clearButton.setTitle("Clear", forState: .Normal)
        }
        else{
            userIDText.text = ""
            hostIPText.text = ""
            passwordText.text = ""
            portNumberText.text = ""
            clearButton.setTitle("Load", forState: .Normal)

        }
    }
    
    // Start Button Session will Save User Input and Begin Session
    @IBAction func startSessionClick(sender: AnyObject) {
        
        if (userIDText.text!.isEmpty || hostIPText.text!.isEmpty || passwordText.text!.isEmpty || portNumberText.text!.isEmpty)
        {
            
            let alertController: UIAlertController = UIAlertController(title: "Missing Information", message: "One or more of the required entries is missing. Please enter all of the required information.", preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil)
            
            alertController.addAction(okAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        
        else{
        let defaults = `NSUserDefaults`.standardUserDefaults()
        
        defaults.setObject(userIDText.text, forKey: "userIDText")
        defaults.setObject(hostIPText.text, forKey: "hostIPText")
        defaults.setObject(passwordText.text, forKey: "passwordText")
        defaults.setObject(portNumberText.text, forKey: "portNumberText")
        defaults.synchronize()
        
        print("User=\(userIDText.text), Host=\(hostIPText.text), Password=\(passwordText.text)")
        }
    }
    
    
    func loadDefaults(){
        let defaults = `NSUserDefaults`.standardUserDefaults()
        userIDText.text = defaults.objectForKey("userIDText") as? String
        hostIPText.text = defaults.objectForKey("hostIPText") as? String
        passwordText.text = defaults.objectForKey("passwordText") as? String
        portNumberText.text = defaults.objectForKey("portNumberText") as? String
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (userIDText.text!.isEmpty || hostIPText.text!.isEmpty || passwordText.text!.isEmpty || portNumberText.text!.isEmpty)
        {
            
            let alertController: UIAlertController = UIAlertController(title: "Missing Information", message: "One or more of the required entries is missing. Please enter all of the required information.", preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: nil)
            
            alertController.addAction(okAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

